<?php

    // namespace Src;
    
    require 'vendor/autoload.php';

    use Dotenv\Dotenv;

    require_once 'System/DatabaseConnector.php';

    $dotenv = new DotEnv(__DIR__);
    $dotenv->load();

    $dbConnection = new DatabaseConnector();
    $dbConnect = $dbConnection -> getConnection();