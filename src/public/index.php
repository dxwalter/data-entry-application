<?php

header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

require "../bootstrap.php";

require_once '../Controller/CountryController.php';
require_once '../Controller/StateController.php';
require_once '../Controller/LgaController.php';
require_once '../Controller/CommunityController.php';
require_once '../Controller/StreetController.php';


require_once '../TableGateways/CountryGateway.php';
require_once '../TableGateways/StateGateway.php';
require_once '../TableGateways/LgaGateway.php';
require_once '../TableGateways/CommunityGateway.php';
require_once '../TableGateways/StreetGateway.php';

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode( '/', $uri );

$endPoint = $uri[4];
$requestMethod = $_SERVER["REQUEST_METHOD"];;

require_once '../System/urlRoute.php';