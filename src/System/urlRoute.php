<?php

    if ($endPoint == 'country') {
        
        $countryController = new CountryController(
            $dbConnect, 
            $requestMethod, 
            $uri,
            new CountryGateway($dbConnect)
        );

        $countryController -> processRequest();

    } else if ($endPoint == 'state') {
        
        $stateController = new StateController (
            $dbConnect, 
            $requestMethod, 
            $uri,
            new StateGateway($dbConnect)
        );

        $stateController -> processRequest();

    } else if ($endPoint == 'lga') {

        $lgaController = new LgaController (
            $dbConnect, 
            $requestMethod, 
            $uri,
            new LgaGateway($dbConnect)
        );

        $lgaController -> processRequest();



    } else if ($endPoint == 'community') {
        
        $communityController = new communityController (
            $dbConnect, 
            $requestMethod, 
            $uri,
            new CommunityGateway($dbConnect)
        );

        $communityController -> processRequest();


    } else if ($endPoint == "street") {
        
        $streetController = new StreetController (
            $dbConnect, 
            $requestMethod, 
            $uri,
            new StreetGateway($dbConnect)
        );

        $streetController -> processRequest();

    } else {
        header("HTTP/1.1 404 Not Found");
        exit();
    }

?>