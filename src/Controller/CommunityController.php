<?php

require_once '../Controller/util.php';

class CommunityController {

    private $requestType;
    private $dbConnection;
    private $userInput;
    private $CountryId;
    private $StateId;
    private $CommunityGateway;
    private $UtilInstance;

    public $communityId;
    public $lgaId;

    public function __construct($db, $requestType, $urlArray, $CommunityGateway)
    {
        $this -> dbConnection = $db;
        $this -> requestType = $requestType;
        $this -> CommunityGateway = $CommunityGateway;
        if (isset($urlArray[5])) $this -> CountryId = $urlArray[5];
        if (isset($urlArray[6])) $this -> StateId = $urlArray[6];
        if (isset($urlArray[7])) $this -> lgaId = $urlArray[7];

        $this -> userInput = json_decode(file_get_contents("php://input"), TRUE);
        
        $this -> UtilInstance = new Util();
    }

    public function processRequest()
    {
        switch ($this->requestType) {
            case 'GET':
                if ($this -> communityId) {
                    $response = $this->getLga($this -> communityId);
                } else {
                    $response = $this->getAllCommunities($this -> CountryId, $this -> StateId, $this -> lgaId);
                };
                break;
            case 'POST':
                $response = $this->createNewCommunities();
                break;
            case 'PUT':
                $response = $this->updateUserFromRequest($this->userId);
                break;
            case 'DELETE':
                $response = $this->deleteUser($this->userId);
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    public function getLga($lgaId)
    {
        $result = $this -> LgaGateway -> oneLga($lgaId, $this -> StateId, $this -> CountryId);
        if (! $result) {
            return $this -> UtilInstance -> notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode([
            "data" => $result
        ]);
        return $response;
    }


    public function getAllCommunities($countryId, $stateId, $lgaId)
    {
        $result = $this -> CommunityGateway -> allCommunities($countryId, $stateId, $lgaId);
        if (! $result) {
            return $this -> UtilInstance -> notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode([
            "data" => $result
        ]);
        return $response;
    }

    public function createNewCommunities()
    {
        $inputData = $this -> userInput['communities'];
        $countryId = $this -> CountryId;
        $stateId = $this -> StateId;
        $lgaId = $this -> lgaId;
        $lgaArray = $this -> UtilInstance -> explodeString($inputData);

        $lgaArrayCount = sizeof($lgaArray) - 1;

        $addcount = 0;

        for ($i = 0; $i <= $lgaArrayCount; $i++) {
            // check if it is not empty
            if (!empty($lgaArray[$i])) {
                // check if it exists in db
                
                if ($this -> CommunityGateway -> checkIfExist($lgaArray[$i], $countryId, $stateId, $lgaId) == false) {
                    // insert into db

                    if ($this -> CommunityGateway -> insert($lgaArray[$i], $countryId,  $stateId, $lgaId) == true) {
                        $addcount += 1;
                    }
                }
            }
        }

        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        http_response_code(201);
        $message = "$addcount community records where added to the database";
        $response['body'] = json_encode(array(
            "message" => $message,
        ));
        return $response;
    }
}