<?php

    class Util {

        public function explodeString($data)
        {   
            $newArray = explode(',', $data);
            $formattedArray = [];

            for ($x = 0; $x <= sizeof($newArray) - 1; ++$x) {
                $formattedArray[$x] = ucfirst(trim($newArray[$x]));
            }

            sort($formattedArray);
            return $formattedArray;
            
        }

        public function notFoundResponse()
        {
            $response['status_code_header'] = 'HTTP/1.1 404 Not Found';
            $response['body'] = "Content not found";
            echo json_encode($response);
        }
    }