<?php

require_once '../Controller/util.php';

class StateController {

    private $requestType;
    private $dbConnection;
    private $userInput;
    private $CountryId;

    private $urlArray;
    private $stateGateway;
    private $UtilInstance;

    public $stateId;

    public function __construct($db, $requestType, $urlArray, $StateGateway)
    {
        $this -> dbConnection = $db;
        $this -> requestType = $requestType;
        $this -> stateGateway = $StateGateway;
        $this -> urlArray = $urlArray;
        $this -> CountryId = $urlArray[5];
        $this -> userInput = json_decode(file_get_contents("php://input"), TRUE);
        
        $this -> UtilInstance = new Util();
    }


    public function processRequest()
    {
        switch ($this->requestType) {
            case 'GET':
                if ($this->stateId) {
                    $response = $this->getState($this->stateId);
                } else {
                    $response = $this->getAllState($this -> CountryId);
                };
                break;
            case 'POST':
                $response = $this->createNewStates();
                break;
            case 'PUT':
                $response = $this->updateUserFromRequest($this->userId);
                break;
            case 'DELETE':
                $response = $this->deleteUser($this->userId);
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    public function getAllState($countryId)
    {
        $result = $this -> stateGateway -> allStates($countryId);
        if (! $result) {
            return $this->notFoundResponse();
        }
        $response['status_code_header'] = 'HTTP/1.1 200 OK';
        $response['body'] = json_encode([
            "data" => $result
        ]);
        return $response;
    }

    public function createNewStates()
    {
        $inputData = $this -> userInput['state'];
        $countryId = $this -> CountryId;
        $stateArray = $this -> UtilInstance -> explodeString($inputData);

        $stateArrayCount = sizeof($stateArray) - 1;

        $addcount = 0;

        for ($i = 0; $i <= $stateArrayCount; $i++) {
            // check if it is not empty
            if (!empty($stateArray[$i])) {
                // check if it exists in db
                if ($this -> stateGateway -> checkIfExist($stateArray[$i], $countryId) == false) {
                    // insert into db
                    
                    if ($this -> stateGateway -> insert($stateArray[$i], $countryId) == true) {
                        $addcount += 1;
                    }
                }
            }
        }

        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        http_response_code(201);
        $message = "$addcount state records where added to the database";
        $response['body'] = json_encode(array(
            "message" => $message,
        ));
        return $response;
    }

}