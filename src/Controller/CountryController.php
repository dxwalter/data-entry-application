<?php

require_once '../Controller/util.php';

class CountryController {

    private $requestType;
    private $dbConnection;
    private $userInput;

    private $urlArray;
    private $CountryGateway;
    private $UtilInstance;

    public function __construct($db, $requestType, $urlArray, $CountryGateway)
    {
        $this -> dbConnection = $db;
        $this -> requestType = $requestType;
        $this -> CountryGateway = $CountryGateway;
        $this -> urlArray = $urlArray;
        $this -> userInput = json_decode(file_get_contents("php://input"), TRUE);
        
        $this -> UtilInstance = new Util();
    }


    public function processRequest()
    {
        switch ($this->requestType) {
            case 'GET':
                if ($this->userId) {
                    $response = $this->getUser($this->userId);
                } else {
                    $response = $this->getAllUsers();
                };
                break;
            case 'POST':
                $response = $this->createNewCountries();
                break;
            case 'PUT':
                $response = $this->updateUserFromRequest($this->userId);
                break;
            case 'DELETE':
                $response = $this->deleteUser($this->userId);
                break;
            default:
                $response = $this->notFoundResponse();
                break;
        }
        header($response['status_code_header']);
        if ($response['body']) {
            echo $response['body'];
        }
    }

    public function createNewCountries()
    {
        $inputData = $this -> userInput['countries'];
        $countriesArray = $this -> UtilInstance -> explodeString($inputData);

        $countriesArrayCount = sizeof($countriesArray);

        $addcount = 0;

        for ($i = 0; $i <= $countriesArrayCount; $i++) {
            // check if it is not empty
            if (!empty($countriesArray[$i])) {
                // check if it exists in db
                if ($this -> CountryGateway -> checkIfExist($countriesArray[$i]) == false) {
                    // insert into db
                    if ($this -> CountryGateway -> insert($countriesArray[$i]) == true) {
                        $addcount += 1;
                    }
                }
            }
        }

        $response['status_code_header'] = 'HTTP/1.1 201 Created';
        http_response_code(201);
        $response['body'] = "$addcount country records where added to the database";
        echo json_encode($response);
    }

}