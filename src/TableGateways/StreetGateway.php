<?php

    class StreetGateway {

        private $db;
        private $tableName = 'streets';

        public $id;
        public $country_id;
        public $state_id;
        public $lga_id;
        public $street_name;

        public function __construct($db)
        {
            $this -> db = $db;
        }

        public function findAll()
        {
            return "find all states";
        }

        public function checkIfExist($streetName, $countryId, $stateId, $lgaId, $communityId)
        {
            $query = $this -> db -> prepare(
                "
                    SELECT * FROM ".$this -> tableName."
                        WHERE street_name = :streetName AND 
                            country_id = :countryId AND 
                                state_id = :stateId AND 
                                    lga_id = :lgaId AND
                                        community_id = :communityId
                                            LIMIT 1
                "
            );

            $query -> bindParam(':streetName', $streetName);
            $query -> bindParam(':countryId',  $countryId);
            $query -> bindParam(':stateId',  $stateId);
            $query -> bindParam(':lgaId',  $lgaId);
            $query -> bindParam(':communityId',  $communityId);
            $query -> execute();

            return $query -> rowCount();
        }

        public function insert($streetName, $countryId, $stateId, $lgaId, $communityId)
        {

            $statement = "
                INSERT INTO ".$this -> tableName." 
                (id, country_id, state_id, lga_id, community_id, street_name, date_created)
                VALUES (?, ?, ?, ?, ?, ?, NOW())
            ";

            $query = $this -> db -> prepare ($statement);

            $query -> execute ([
                "",
                $countryId,
                $stateId,
                $lgaId,
                $communityId,
                $streetName
            ]);

            if ($query -> rowCount()) {
                return true;
            } else {
                return false;
            }
        }

        public function allCommunities ($countryId, $stateId, $lgaId)  
        {
            $statement = "
                SELECT id, community_name FROM ".$this -> tableName." WHERE country_id = :countryId AND
                state_id = :stateId AND
                lga_id = :lgaId";

            $query = $this -> db -> prepare($statement);

            $query -> bindParam(':countryId',  $countryId);
            $query -> bindParam(':stateId',  $stateId);
            $query -> bindParam(':lgaId',  $lgaId);
            $query -> execute();

            $result = $query->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        }
    }

?>