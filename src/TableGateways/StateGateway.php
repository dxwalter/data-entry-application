<?php

    class StateGateway {

        private $db;
        private $tableName = 'state';

        public $id;
        public $country_id;
        public $state_name;

        public function __construct($db)
        {
            $this -> db = $db;
        }

        public function findAll()
        {
            return "find all states";
        }

        public function checkIfExist($stateName, $countryId)
        {
            $query = $this -> db -> prepare(
                "
                    SELECT * FROM ".$this -> tableName."
                        WHERE state_name = :stateName AND country_id = :countryId
                            LIMIT 1
                "
            );

            $query -> bindParam(':stateName', $stateName);
            $query -> bindParam(':countryId',  $countryId);
            $query -> execute();

            return $query -> rowCount();
        }

        public function insert($stateName, $countryId)
        {

            $query = $this -> db -> prepare ("
                INSERT into ".$this -> tableName." 
                (id, country_id, state_name, date_added)
                VALUES (?, ?, ?, NOW())
            ");

            $query -> execute ([
                "",
                $countryId,
                $stateName
            ]);

            if ($query) {
                return true;
            } else {
                return false;
            }
        }

        public function allStates ($countryId)  
        {
            $statement = "SELECT id, state_name FROM ".$this -> tableName." WHERE country_id = :countryId AND status = :status";

            $query = $this -> db -> prepare($statement);

            $status = 1;
            $query -> bindParam(':countryId',  $countryId);
            $query -> bindParam(':status',  $status);
            $query -> execute();

            $result = $query->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        }
    }

?>