<?php


class CountryGateway {

    private $db;
    private $tableName = 'countries';

    public $id;
    public $name;

    public function __construct($db)
    {
        $this -> db = $db;
    }

    public function findAll()
    {
        return "find all countries";
    }

    public function checkIfExist($countryName)
    {
        $query = $this -> db -> prepare(
            "
                SELECT * FROM ".$this -> tableName."
                    WHERE name = :countryName LIMIT 1
            "
        );

        $query -> bindParam(':countryName', $countryName);
        $query -> execute();

        return $query -> rowCount();
    }

    public function insert($countrName)
    {
        $query = $this -> db -> prepare ("
            INSERT into ".$this -> tableName." 
            (id, name, date_added)
            VALUES (?, ?, NOW())
        ");

        $query -> execute ([
            "",
            $countrName
        ]);

        if ($query -> rowCount()) {
            return true;
        } else {
            return false;
        }
    }
}

?>