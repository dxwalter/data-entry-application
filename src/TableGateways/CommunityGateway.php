<?php

    class CommunityGateway {

        private $db;
        private $tableName = 'communities';

        public $id;
        public $country_id;
        public $state_id;
        public $lga_id;
        public $community_name;

        public function __construct($db)
        {
            $this -> db = $db;
        }

        public function findAll()
        {
            return "find all states";
        }

        public function checkIfExist($communityName, $countryId, $stateId, $lgaId)
        {
            $query = $this -> db -> prepare(
                "
                    SELECT * FROM ".$this -> tableName."
                        WHERE community_name = :communityName AND 
                            country_id = :countryId AND 
                                state_id = :stateId AND 
                                    lga_id = :lgaId
                                        LIMIT 1
                "
            );

            $query -> bindParam(':communityName', $communityName);
            $query -> bindParam(':countryId',  $countryId);
            $query -> bindParam(':stateId',  $stateId);
            $query -> bindParam(':lgaId',  $lgaId);
            $query -> execute();

            return $query -> rowCount();
        }

        public function insert($communityName, $countryId, $stateId, $lgaId)
        {

            $statement = "
                INSERT INTO ".$this -> tableName." 
                (id, country_id, state_id, lga_id, community_name, date_created)
                VALUES (?, ?, ?, ?, ?, NOW())
            ";

            $query = $this -> db -> prepare ($statement);

            $query -> execute ([
                "",
                $countryId,
                $stateId,
                $lgaId,
                $communityName
            ]);

            if ($query -> rowCount()) {
                return true;
            } else {
                return false;
            }
        }

        public function allCommunities ($countryId, $stateId, $lgaId)  
        {
            $statement = "
                SELECT id, community_name FROM ".$this -> tableName." WHERE country_id = :countryId AND
                state_id = :stateId AND
                lga_id = :lgaId";

            $query = $this -> db -> prepare($statement);

            $query -> bindParam(':countryId',  $countryId);
            $query -> bindParam(':stateId',  $stateId);
            $query -> bindParam(':lgaId',  $lgaId);
            $query -> execute();

            $result = $query->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        }
    }

?>