<?php

    class LgaGateway {

        private $db;
        private $tableName = 'lga';

        public $id;
        public $country_id;
        public $state_id;
        public $lga_name;

        public function __construct($db)
        {
            $this -> db = $db;
        }

        public function findAll()
        {
            return "find all states";
        }

        public function checkIfExist($lgaName, $countryId, $stateId)
        {
            $query = $this -> db -> prepare(
                "
                    SELECT * FROM ".$this -> tableName."
                        WHERE lga_name = :lgaName AND country_id = :countryId AND state_id = :stateId
                            LIMIT 1
                "
            );

            $query -> bindParam(':lgaName', $lgaName);
            $query -> bindParam(':countryId',  $countryId);
            $query -> bindParam(':stateId',  $stateId);
            $query -> execute();

            return $query -> rowCount();
        }

        public function insert($lgaName, $countryId, $stateId)
        {

            $query = $this -> db -> prepare ("
                INSERT into ".$this -> tableName." 
                (id, country_id, state_id, lga_name, date_added)
                VALUES (?, ?, ?, ?, NOW())
            ");

            $query -> execute ([
                "",
                $countryId,
                $stateId,
                $lgaName
            ]);

            if ($query) {
                return true;
            } else {
                return false;
            }
        }

        public function allLgas ($stateId)  
        {
            $statement = "SELECT id, lga_name FROM ".$this -> tableName." WHERE state_id = :stateId";

            $query = $this -> db -> prepare($statement);

            $query -> bindParam(':stateId',  $stateId);
            $query -> execute();

            $result = $query->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
        }

        public function oneLga($lgaId, $stateId, $countryId) {

            $statement = "SELECT * FROM ".$this -> tableName." WHERE id = :lgaId AND state_id = :stateId AND country_id = :countryId";

            $query = $this -> db -> prepare($statement);

            $query -> bindParam(':lgaId',  $lgaId);
            $query -> bindParam(':stateId',  $stateId);
            $query -> bindParam(':countryId',  $countryId);
            
            $query -> execute();

            $result = $query->fetchAll(\PDO::FETCH_ASSOC);
            return $result;
            
        }
    }

?>