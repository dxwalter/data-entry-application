
let outputElement;

let countriesCallback = function (response) {
    alert(response.body)
}

let ajaxFunction = (type, url, data) => {
    $.ajax({
        type: type,
        url: url,
        cache: false,
        data: data,
        success:  function (response) {
            console.log(response.responseText.message)
        },
        error: function (response) {
            console.log(response);
        }
})
}

let lgaSelectCallback = (response) => {
    let resMessage = response.data;
    resMessage.forEach((element, index, array) => {
        let val = element.id;
        let lgaName = element.lga_name;
        let lga = document.getElementById(outputElement);
        let option = document.createElement("option");
        option.innerHTML = lgaName;
        option.value = val;

        lga.appendChild(option);
    });
}

let selectFormCallBack = (response) => {
    let resMessage = response.data;
    resMessage.forEach((element, index, array) => {
        let val = element.id;
        let stateName = element.state_name;
        let state = document.getElementById(outputElement);
        let option = document.createElement("option");
        option.innerHTML = stateName;
        option.value = val;

        state.appendChild(option);
    });
}


let communitySelectCb = (response) => {
    let resMessage = response.data;
    resMessage.forEach((element, index, array) => {
        let val = element.id;
        let commName = element.community_name;
        let state = document.getElementById(outputElement);
        let option = document.createElement("option");
        option.innerHTML = commName;
        option.value = val;

        state.appendChild(option);
    });
}

let ajaxFillFunction = (url, callBackFunction) => {
    $.ajax({
        type: "GET",
        url: url,
        cache: false,
        data: "",
        success: callBackFunction,
        error: function (response) {
            console.log(response.responseText);
        }
    })
}


let submitCountry = $('#subCountry');
submitCountry.on('click', function () {
    let countryData = $('#countryContent').val();

    let url = "http://localhost/location/src/public/country";
    let data = JSON.stringify({
        "countries": countryData
    });

    ajaxFunction("POST", url, data);
    // call ajax fuction and pass in url

});


let subState = $("#subState");
subState.on('click', function () {
    let stateCountry = $('#stateCountry').val();
    let stateContent = $('#stateContent').val();

    let url = "http://localhost/location/src/public/state/"+stateCountry;
    let data = JSON.stringify({
        "state": stateContent
    });

    ajaxFunction("POST", url, data);
    // call ajax fuction and pass in url

});


let subLga = $('#subLga');
subLga.on('click', function () {
    let country = $('#lgaCountry').val();
    let state = $('#lgaState').val();
    let lga = $('#lgaContent').val();

    let url = "http://localhost/location/src/public/lga/"+country+"/"+state;
    let data = JSON.stringify({
        "lga": lga
    });

    ajaxFunction("POST", url, data);
})

let subCom = $('#subCom');
subCom.on('click', function() {
    let country = $('#comCountry').val();
    let state = $('#comState').val();
    let lga = $('#comLga').val();
    let community = $('#comContent').val();

    let url = "http://localhost/location/src/public/community/"+country+"/"+state+"/"+lga;
    let data = JSON.stringify({
        "communities" : community
    });

    ajaxFunction("POST", url, data);

});

let strSubmit = $('#strSubmit');
strSubmit.on('click', function () {
    let country = $('#strCountry').val();
    let state = $('#strState').val();
    let lga = $('#strLga').val();
    let community = $('#strCom').val();
    let street = $('#strContent').val();

    let url = "http://localhost/location/src/public/street/"+country+"/"+state+"/"+lga+"/"+community;
    let data = JSON.stringify({
        "streets" : street
    });

    ajaxFunction("POST", url, data);
})


function countryGetsState(countryFormId, stateId) {
    let countryState = document.getElementById(countryFormId);

    let countryId = countryState.value;
    outputElement = stateId;
    let url = "http://localhost/location/src/public/state/"+countryId;
    ajaxFillFunction(url, selectFormCallBack);
}


let stateGetsLga = (stateId, lgaId) => {
    let state = document.getElementById(stateId);
    let countryId = document.getElementById('comCountry').value;
    let stateForm = state.value;
    outputElement = lgaId;
    let url = "http://localhost/location/src/public/lga/"+countryId+"/"+stateForm;
    ajaxFillFunction(url, lgaSelectCallback);

}


let lgaGetsCommunities = (lgaId, commId) => {
    let lga = document.getElementById(lgaId).value;
    let countryId = document.getElementById('strCountry').value;
    let stateForm = document.getElementById('strState').value;
    outputElement = commId;
    let url = "http://localhost/location/src/public/community/"+countryId+"/"+stateForm+"/"+lga;
    ajaxFillFunction(url, communitySelectCb);
}